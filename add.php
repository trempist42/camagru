<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
    <title>Camagru! | Add photo</title>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat" />
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat+Alternates" />
    <link rel="stylesheet" type="text/css" href="styles/style.css" />
	<link rel="stylesheet" type="text/css" href="styles/add.css" />
    <?php session_start(); if (!isset($_SESSION['login'])) require_once('back/denyaccess.php'); if (isset($_GET['logout']) && $_GET['logout'] === 'true') {require_once('back/logout.php');}?>
</head>
<body>
<div class="wrapper">
    <div class="header">
        <?php require_once('back/header.php');?>
    </div>
    <div class="content">
        <div class="main">
            <h2>Add photo:</h2>
            <div id="add">
                <div id="video">
					<p>Select a photo:</p>
                    <div id="supervideodiv">
                        <div id="videodiv">
                            <video playsinline autoplay style="display: none"></video>
                        </div>
                        <img class="superposablevid" style="left: 0; top: 0; display: none;" src="superposable/sun.gif" alt="sun"/>
                        <img class="superposablevid" style="left: 0; top: 0; display: none;" src="superposable/duck.gif" alt="duck"/>
                    </div>
                    <button>Take snapshot</button>
					<p style="display: none">Can't get access to your camera!</p>
					<p>or select your own image:</p>
					<input type="file" accept="image/*">
                </div>
                <div id="cnvs">
					<p id="warning" style="display: none"><b>Warning!</b> It seems you're uploading a gif image. If you'll modify the animated gif image, only first frame will be stored.</p>
                    <canvas id="canvas" style="display: none"></canvas>
					<div id="imgdiv" style="display: none">
						<div id="img">
                            <img id="preview" style=""/>
                        </div>
						<img class="superposable" style="left: 0; top: 0; display: none;" src="superposable/sun.gif" alt="sun"/>
						<img class="superposable" style="left: 0; top: 0; display: none;" src="superposable/duck.gif" alt="duck"/>
					</div>
					<div class="superposableimgs">
						Superposable images:
						<div id="superposableimg">
							<img src="superposable/sun.gif" alt="sun">
							<img src="superposable/duck.gif" alt="duck">
						</div>
					</div>
					<input type="submit" name="Submit" style="display: none">
                </div>
            </div>
        </div>
		<div class="side">
			<h2>Your photos:</h2>
			<?php require_once('back/side.php');?>
		</div>
    </div>
    <div class="footer">
        <h5>Copyright © 2019 rafalmer</h5>
    </div>
</div>
<script src="scripts/add.js"></script>
</body>
</html>