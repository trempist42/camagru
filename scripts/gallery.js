const img = document.getElementsByClassName('img');
const likeimg = document.getElementsByClassName('likeimg');
const commentimg = document.getElementsByClassName('commentimg');
const imagediv = document.getElementsByClassName('divimg');
const video = document.getElementsByTagName('video')[0];
const videodiv = document.getElementById('videodiv');

function resizeImagediv() {
	for (let i = 0; i < imagediv.length; i++) {
		imagediv[i].style.height = null;
		imagediv[i].style.width = null;
		imagediv[i].style.height = img[i].height + 'px';
		imagediv[i].style.width = img[i].width + 'px';
	}
}

window.addEventListener('resize', function () {
	resizeImagediv();
});

window.addEventListener('load', function () {
	resizeImagediv();
});

window.addEventListener('orientationchange', function () {
	resizeImagediv();
});

if (document.getElementsByClassName('loggedin')[0] !== undefined)
{
	Array.prototype.forEach.call(img, function (element) {
		element.onclick = function () {
			let id = element.parentElement.parentElement.innerHTML.match(/id="(.*?)"/)[1];
			location.href = '/photo.php?id=' + id;
		};
	});
	Array.prototype.forEach.call(likeimg, function(element) {
		element.onclick = function () {
			let likes = element.parentElement.parentElement.getElementsByClassName('likes')[0];
			let likesnum = likes.innerText.match(/\d+/g).map(Number)[0];
			let xmlhttp = new XMLHttpRequest();
			xmlhttp.open("POST","/interact.php",true);
			xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xmlhttp.send('like=' + element.id);
			if (element.src.search(/like.svg/) !== -1)
			{
				likesnum++;
				element.src = 'icons/liked.svg';
				if (likesnum === 1)
				{
					likes.innerText = likesnum + ' like';
				}
				else
				{
					likes.innerText = likesnum + ' likes';
				}
			}
			else
			{
				likesnum--;
				element.src = 'icons/like.svg';
				if (likesnum === 1)
				{
					likes.innerText = likesnum + ' like';
				}
				else
				{
					likes.innerText = likesnum + ' likes';
				}
			}
		};
	});
	Array.prototype.forEach.call(commentimg, function (element) {
		element.onclick = function () {
			let id = element.parentElement.parentElement.innerHTML.match(/id="(.*?)"/)[1];
			location.href = '/photo.php?id=' + id + '&comment';
		};
	});
}
else
{
	Array.prototype.forEach.call(img, function(element) {
		element.onclick = function () {
			alert('Please, sign in first!');
		};
	});
	Array.prototype.forEach.call(likeimg, function(element) {
		element.onclick = function () {
			alert('Please, sign in first!');
		};
	});
	Array.prototype.forEach.call(commentimg, function(element) {
		element.onclick = function () {
			alert('Please, sign in first!');
		};
	});
}
let pagination = document.getElementsByClassName('pagination')[0];
if (pagination !== undefined)
{
	pagination = pagination.childNodes;
	pagination[0].style.borderLeft = '1px solid #ccc';
	pagination[0].style.borderTopLeftRadius = '100%';
	pagination[0].style.borderBottomLeftRadius = '100%';
	pagination[pagination.length - 1].style.borderTopRightRadius = '100%';
	pagination[pagination.length - 1].style.borderBottomRightRadius = '100%';
}
