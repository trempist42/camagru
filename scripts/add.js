'use strict';

const video = document.querySelector('video');
const canvas = window.canvas = document.querySelector('canvas');
const cnvs = document.getElementById('cnvs');
const button = document.querySelector('button');
const file = document.getElementById('video').querySelector('input');
const submit = document.getElementById('cnvs').querySelector('input');
const preview = document.getElementById('preview');
const p = document.getElementById('video').getElementsByTagName('p');
const reader = new FileReader();
const img = document.getElementsByClassName('img');
const spimages = document.getElementById('superposableimg').getElementsByTagName('img');
const superposable = document.getElementsByClassName('superposable');
const superposablevid = document.getElementsByClassName('superposablevid');
const imgdiv = document.getElementById('img');
const imagediv = document.getElementsByClassName('imagediv');
const fileTypes = ['jpg', 'jpeg', 'bmp', 'png', 'gif'];
const warning = document.getElementById('warning');
const supervideodiv = document.getElementById('supervideodiv');
const divimg = document.getElementById('imgdiv');

let superposables = {
	sun: {
		set: 0,
		left: 0,
		top: 0,
		width: 0,
		height: 0
	},
	duck: {
		set: 0,
		left: 0,
		top: 0,
		width: 0,
		height: 0
	}
};
let mousePosition;
let touchPosition;
let offset = [0,0];
let isDown = false;
let isStart = false;

preview.addEventListener('mousemove', function (event) {
	event.preventDefault();
}, {passive: false});

function resizeImagediv() {
	for (let i = 0; i < imagediv.length; i++) {
		imagediv[i].style.height = null;
		imagediv[i].style.width = null;
		imagediv[i].style.height = img[i].height + 'px';
		imagediv[i].style.width = img[i].width + 'px';
	}
	videodiv.style.height = null;
	videodiv.style.width = null;
	videodiv.style.height = video.offsetHeight + 'px';
	videodiv.style.width = video.offsetWidth + 'px';
	if (preview.src !== '') {
		imgdiv.style.height = null;
		imgdiv.style.width = null;
		imgdiv.style.height = preview.height + 'px';
		imgdiv.style.width = preview.width + 'px';
		Array.prototype.forEach.call(imagediv, function (div) {
		});
	}
}

video.addEventListener('canplaythrough', function () {
	resizeImagediv();
});

window.addEventListener('load', function () {
	resizeImagediv();
});

window.addEventListener('resize', function () {
	resizeImagediv();
});


window.addEventListener('orientationchange', function () {
	resizeImagediv();
	if (preview.src !== '') {
		imgdiv.style.height = null;
		imgdiv.style.width = null;
		imgdiv.style.height = preview.height + 'px';
		imgdiv.style.width = preview.width + 'px';
	}
});

preview.addEventListener('load', function () {
	if (preview.src !== '') {
		imgdiv.style.height = null;
		imgdiv.style.width = null;
		imgdiv.style.height = preview.height + 'px';
		imgdiv.style.width = preview.width + 'px';
	}
});

superposable[0].addEventListener('mousedown', function(event) {
	isDown = true;
	offset = [
		superposable[0].offsetLeft - event.clientX,
		superposable[0].offsetTop - event.clientY
	];
});

superposable[0].addEventListener('touchstart', function(event) {
	isStart = true;
	offset = [
		superposable[0].offsetLeft - event.targetTouches[0].clientX,
		superposable[0].offsetTop - event.targetTouches[0].clientY
	];
}, {passive: false});

superposable[0].addEventListener('mouseup', function(event) {
	isDown = false;
});

superposable[0].addEventListener('touchend', function(event) {
	isStart = false;
});

superposable[0].addEventListener('mousemove', function(event) {
	event.preventDefault();
	if (isDown) {
		mousePosition = {

			x : event.clientX,
			y : event.clientY

		};
		superposable[0].style.left = (mousePosition.x + offset[0]) + 'px';
		superposable[0].style.top  = (mousePosition.y + offset[1]) + 'px';
		superposablevid[0].style.left = (mousePosition.x + offset[0]) + 'px';
		superposablevid[0].style.top  = (mousePosition.y + offset[1]) + 'px';
	}
}, {passive: false});

superposable[0].addEventListener('touchmove', function(event) {
	event.preventDefault();
	event.stopPropagation();
	if (isStart) {
		touchPosition = {

			x : event.targetTouches[0].clientX,
			y : event.targetTouches[0].clientY

		};
		superposable[0].style.left = (touchPosition.x + offset[0]) + 'px';
		superposable[0].style.top  = (touchPosition.y + offset[1]) + 'px';
		superposablevid[0].style.left = (touchPosition.x + offset[0]) + 'px';
		superposablevid[0].style.top  = (touchPosition.y + offset[1]) + 'px';
	}
});

superposable[1].addEventListener('mousedown', function(event) {
	isDown = true;
	offset = [
		superposable[1].offsetLeft - event.clientX,
		superposable[1].offsetTop - event.clientY
	];
});

superposable[1].addEventListener('touchstart', function(event) {
	isStart = true;
	offset = [
		superposable[1].offsetLeft - event.targetTouches[0].clientX,
		superposable[1].offsetTop - event.targetTouches[0].clientY
	];
}, {passive: false});

superposable[1].addEventListener('mouseup', function(event) {
	isDown = false;
});

superposable[1].addEventListener('touchend', function(event) {
	isStart = false;
});

superposable[1].addEventListener('mousemove', function(event) {
	event.preventDefault();
	if (isDown) {
		mousePosition = {

			x : event.clientX,
			y : event.clientY

		};
		superposable[1].style.left = (mousePosition.x + offset[0]) + 'px';
		superposable[1].style.top  = (mousePosition.y + offset[1]) + 'px';
		superposablevid[1].style.left = (mousePosition.x + offset[0]) + 'px';
		superposablevid[1].style.top  = (mousePosition.y + offset[1]) + 'px';
	}
}, {passive: false});

superposable[1].addEventListener('touchmove', function(event) {
	event.preventDefault();
	event.stopPropagation();
	if (isStart) {
		touchPosition = {

			x : event.targetTouches[0].clientX,
			y : event.targetTouches[0].clientY

		};
		superposable[1].style.left = (touchPosition.x + offset[0]) + 'px';
		superposable[1].style.top  = (touchPosition.y + offset[1]) + 'px';
		superposablevid[1].style.left = (touchPosition.x + offset[0]) + 'px';
		superposablevid[1].style.top  = (touchPosition.y + offset[1]) + 'px';
	}
});

superposablevid[0].addEventListener('mousedown', function(event) {
	isDown = true;
	offset = [
		superposablevid[0].offsetLeft - event.clientX,
		superposablevid[0].offsetTop - event.clientY
	];
});

superposablevid[0].addEventListener('touchstart', function(event) {
	isStart = true;
	offset = [
		superposablevid[0].offsetLeft - event.targetTouches[0].clientX,
		superposablevid[0].offsetTop - event.targetTouches[0].clientY
	];
}, {passive: false});

superposablevid[0].addEventListener('mouseup', function(event) {
	isDown = false;
});

superposablevid[0].addEventListener('touchend', function(event) {
	isStart = false;
});

superposablevid[0].addEventListener('mousemove', function(event) {
	event.preventDefault();
	if (isDown) {
		mousePosition = {

			x : event.clientX,
			y : event.clientY

		};
		superposablevid[0].style.left = (mousePosition.x + offset[0]) + 'px';
		superposablevid[0].style.top  = (mousePosition.y + offset[1]) + 'px';
		superposable[0].style.left = (mousePosition.x + offset[0]) + 'px';
		superposable[0].style.top  = (mousePosition.y + offset[1]) + 'px';
	}
}, {passive: false});

superposablevid[0].addEventListener('touchmove', function(event) {
	event.preventDefault();
	event.stopPropagation();
	if (isStart) {
		touchPosition = {

			x : event.targetTouches[0].clientX,
			y : event.targetTouches[0].clientY

		};
		superposablevid[0].style.left = (touchPosition.x + offset[0]) + 'px';
		superposablevid[0].style.top  = (touchPosition.y + offset[1]) + 'px';
		superposable[0].style.left = (touchPosition.x + offset[0]) + 'px';
		superposable[0].style.top  = (touchPosition.y + offset[1]) + 'px';
	}
});

superposablevid[1].addEventListener('mousedown', function(event) {
	isDown = true;
	offset = [
		superposablevid[1].offsetLeft - event.clientX,
		superposablevid[1].offsetTop - event.clientY
	];
});

superposablevid[1].addEventListener('touchstart', function(event) {
	isStart = true;
	offset = [
		superposablevid[1].offsetLeft - event.targetTouches[0].clientX,
		superposablevid[1].offsetTop - event.targetTouches[0].clientY
	];
}, {passive: false});

superposablevid[1].addEventListener('mouseup', function(event) {
	isDown = false;
});

superposablevid[1].addEventListener('touchend', function(event) {
	isStart = false;
});

superposablevid[1].addEventListener('mousemove', function(event) {
	event.preventDefault();
	if (isDown) {
		mousePosition = {

			x : event.clientX,
			y : event.clientY

		};
		superposablevid[1].style.left = (mousePosition.x + offset[0]) + 'px';
		superposablevid[1].style.top  = (mousePosition.y + offset[1]) + 'px';
		superposable[1].style.left = (mousePosition.x + offset[0]) + 'px';
		superposable[1].style.top  = (mousePosition.y + offset[1]) + 'px';
	}
}, {passive: false});

superposablevid[1].addEventListener('touchmove', function(event) {
	event.preventDefault();
	event.stopPropagation();
	if (isStart) {
		touchPosition = {

			x : event.targetTouches[0].clientX,
			y : event.targetTouches[0].clientY

		};
		superposablevid[1].style.left = (touchPosition.x + offset[0]) + 'px';
		superposablevid[1].style.top  = (touchPosition.y + offset[1]) + 'px';
		superposable[1].style.left = (touchPosition.x + offset[0]) + 'px';
		superposable[1].style.top  = (touchPosition.y + offset[1]) + 'px';
	}
});

Array.prototype.forEach.call(spimages, function (element) {
	element.onclick = function () {
		let id = element.src.match(/superposable\/(.*?)$/)[1];
		if (id === 'sun.gif')
		{
			if (superposable[0].style.display === 'none' || superposablevid[0].style.display === 'none')
			{
				superposable[0].style.display = '';
				superposablevid[0].style.display = '';
				superposables['sun']['set'] = 1;
			}
			else if (superposable[0].style.display === '' || superposablevid[0].style.display === '')
			{
				superposable[0].style.display = 'none';
				superposablevid[0].style.display = 'none';
				superposable[0].style.left = '0';
				superposablevid[0].style.left = 'none';
				superposable[0].style.top = '0';
				superposablevid[0].style.top = 'none';
				superposables['sun']['set'] = 0;
			}
		}
		else if (id === 'duck.gif')
		{
			if (superposable[1].style.display === 'none' || superposablevid[1].style.display === 'none')
			{
				superposable[1].style.display = '';
				superposablevid[1].style.display = '';
				superposables['duck']['set'] = 1;
			}
			else if (superposable[1].style.display === '' || superposablevid[1].style.display === '')
			{
				superposable[1].style.display = 'none';
				superposable[1].style.left = '0';
				superposable[1].style.top = '0';
				superposablevid[1].style.display = 'none';
				superposablevid[1].style.left = '0';
				superposablevid[1].style.top = '0';
				superposables['duck']['set'] = 0;
			}
		}
	};
});

Array.prototype.forEach.call(img, function (element) {
	element.onclick = function () {
		let id = element.parentElement.innerHTML.match(/src="images\/(.*?)"/)[1];
		location.href = '/photo.php?id=' + id;
	};
});

submit.onclick = function() {
	superposables['sun']['left'] = parseInt(superposable[0].style.left);
	superposables['sun']['top'] = parseInt(superposable[0].style.top);
	superposables['sun']['width'] = superposable[0].width;
	superposables['sun']['height'] = superposable[0].height;
	superposables['duck']['left'] = parseInt(superposable[1].style.left);
	superposables['duck']['top'] = parseInt(superposable[1].style.top);
	superposables['duck']['width'] = superposable[1].width;
	superposables['duck']['height'] = superposable[1].height;

	if (preview.style.display === 'none') {
		alert('Please, select image file or take snapshot.');
		return;
	}
	let xhr = new XMLHttpRequest();
	xhr.open("POST", '/get_photo.php', true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
	xhr.onreadystatechange=function()
	{
		if (xhr.readyState === 4)
		{
			if (xhr.status === 200)
			{
				alert(xhr.responseText);
			}
			else if (xhr.status === 500)
			{
				alert(xhr.responseText);
			}
		}
	};
	let newImg = new Image();

	newImg.onload = function() {
		let height = newImg.height;
		let width = newImg.width;
		xhr.send(JSON.stringify({
			file: preview.src,
			realSize: {height: height, width: width},
			size: {height: parseInt(preview.height), width: parseInt(preview.width)},
			superposables: superposables
		}));
		alert('Please wait, your image is uploading now..');
	};

	newImg.src = preview.src;
};

button.onclick = function() {
	if (divimg.style.display === 'none') {
		divimg.style.display = '';
		submit.style.display = '';
	}
	if (supervideodiv.style.display === 'none') {
		supervideodiv.style.display = '';
	}
    canvas.height = video.videoHeight;
    canvas.width = video.videoWidth;
    canvas.getContext('2d').drawImage(video, 0, 0);
    preview.src = canvas.toDataURL();
};

file.onchange = function (event) {
	if (divimg.style.display === 'none') {
		divimg.style.display = '';
		submit.style.display = '';
	}
	if (supervideodiv.style.display === '') {
		supervideodiv.style.display = 'none';
	}
	if (event.target.files && event.target.files[0]) {
		let extension = event.target.files[0].name.split('.').pop().toLowerCase(),
			isSuccess = fileTypes.indexOf(extension) > -1;

		if (isSuccess) {
			reader.readAsDataURL(event.target.files[0]);
			reader.onloadend = function() {
				if (extension === 'gif') {
					warning.style.display = null;
				} else {
					warning.style.display = 'none';
				}
				preview.src = reader.result;
			};
		}
		else {
			alert('Image format wrong! Camagru supports only bmp, png, jpeg and gif images.')
		}
	}
};


const constraints = {
    audio: false,
    video: true
};

function handleSuccess(stream) {
    window.stream = stream;
    video.srcObject = stream;
    video.style.display = '';
}

function handleError() {
    button.style.display = 'none';
    p[1].style.display = '';
    p[2].textContent = 'Please, select image (max size - 5MB):';
}

navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);