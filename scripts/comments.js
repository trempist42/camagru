let img = document.getElementsByClassName('img');
let likeimg = document.getElementsByClassName('likeimg');
let commentimg = document.getElementsByClassName('commentimg');
let comments = document.getElementsByClassName('commentsDiv')[0];
let commentsnum = document.getElementsByClassName('comments')[0];
let del = document.getElementById('del');

if (del !== null)
	del.onclick = function () {
		let xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST", "/interact.php", true);
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xmlhttp.send('del=true&imageid=' + likeimg[0].id);
		xmlhttp.onreadystatechange = function () {
			if (xmlhttp.readyState === 4)
			{
				alert(xmlhttp.responseText);
				if (xmlhttp.status === 200)
				{
					location.href = '/';
				}
			}
		};
	};

Array.prototype.forEach.call(likeimg, function(element) {
	element.onclick = function () {
		let likes = element.parentElement.parentElement.getElementsByClassName('likes')[0];
		let likesnum = likes.innerText.match(/\d+/g).map(Number)[0];
		let xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST","/interact.php",true);
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xmlhttp.send('like=' + element.id);
		if (element.src.search(/like.svg/) !== -1)
		{
			likesnum++;
			element.src = 'icons/liked.svg';
			if (likesnum === 1)
			{
				likes.innerText = likesnum + ' like';
			}
			else
			{
				likes.innerText = likesnum + ' likes';
			}
		}
		else
		{
			likesnum--;
			element.src = 'icons/like.svg';
			if (likesnum === 1)
			{
				likes.innerText = likesnum + ' like';
			}
			else
			{
				likes.innerText = likesnum + ' likes';
			}
		}
	};
});

Array.prototype.forEach.call(commentimg, function (element) {
	element.onclick = function () {
		comment.removeAttribute('readonly');
		comment.value = '';
		comment.style.color = '';
		comment.focus();
	};
});



function formSubmit() {
	if (comment.value !== '' && (comment.style.color !== 'grey' || comment.value !== 'Type your comment here (Maximum 140 symbols)'))
	{
		let xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST","/interact.php",true);
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xmlhttp.send('comment=' + comment.value + '&imageid=' + likeimg[0].id);
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState === 4)
			{
				if (xmlhttp.status === 200)
				{
					let commentDiv = document.createElement('div');
					let commentText = document.createElement('div');
					let author = document.createElement('h6');
					let date = document.createElement('h6');
					let response = JSON.parse(xmlhttp.responseText);
					let timestamp = new Date(response['date'] * 1000);
					let numComments = commentsnum.innerText.match(/\d+/g).map(Number)[0];
					commentDiv.className = 'commentDiv';
					commentText.className = 'commentText';
					author.className = 'author';
					date.className = 'date';
					commentDiv.appendChild(author);
					commentDiv.appendChild(commentText);
					commentDiv.appendChild(date);
					author.innerText = 'Author: ' + response['author'];
					commentText.innerText = response['comment'];
					let day = timestamp.getDate();
					if (day.toString().length !== 2)
						day = '0' + day;
					let month = timestamp.getMonth() + 1;
					if (month.toString().length !== 2)
						month = '0' + month;
					let year = timestamp.getFullYear();
					let hour = timestamp.getHours();
					let minute = timestamp.getMinutes();
					if (minute.toString().length !== 2)
						minute = '0' + minute;
					date.innerText = day + '.' + month + '.' + year + ' ' + hour + ':' + minute;
					comments.prepend(commentDiv);
					numComments++;
					if (numComments === 1)
					{
						commentsnum.innerText = numComments + ' comment';
					}
					else
					{
						commentsnum.innerText = numComments + ' comments';
					}
				}
				else if (xmlhttp.status === 500)
				{
					alert('Internal Server Error!\n' + xmlhttp.responseText);
				}
			}
		};
	}
	else
	{
		alert('Comment must contain between 1 and 140 symbols.');
	}
}

document.onkeyup = function(keyEvent) {
	if (comment === document.activeElement)
	{
		if (keyEvent.ctrlKey && keyEvent.key === 'Enter') {
			formSubmit();
		}
	}
};

let comment = document.getElementsByClassName('commentarea')[0];
comment.onclick = function() {
	if (comment.value === 'Type your comment here (Maximum 140 symbols)' && comment.style.color === 'grey')
	{
		comment.value = '';
		comment.style.color = '';
		comment.removeAttribute('readonly');
	}
};
comment.onblur = function() {
	if (comment.value === '' && comment.style.color === '')
	{
		comment.value = 'Type your comment here (Maximum 140 symbols)';
		comment.style.color = 'grey';
		comment.setAttribute('readonly', '');
	}
};
comment.value = 'Type your comment here (Maximum 140 symbols)';
comment.style.color = 'grey';

let submit = document.getElementsByName('submit')[0];
submit.onclick = function () {
	formSubmit();
};