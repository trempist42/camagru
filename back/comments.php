<?php
if (isset($_SESSION['login']) && isset($_GET['id']) && $_GET['id'] !== '')
{
	try
	{
		require_once('config/database.php');
		$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT COUNT(*) AS `count`, `login` FROM `gallery` WHERE `id` LIKE :id');
		$stmt->bindParam(':id', $id);
		$id = $_GET['id'];
		$stmt->execute();
		$res= $stmt->fetch();
		if ($res[0] !== '1')
			require_once('back/denyaccess.php');
		$author = htmlspecialchars($res[1]);
		$stmt = $conn->prepare('SELECT COUNT(*) AS `count` FROM `likes` WHERE `login` LIKE :login AND `imageid` LIKE :id');
		$stmt->bindParam(':login', $login);
		$stmt->bindParam(':id', $id);
		$login = $_SESSION['login'];
		$id = $_GET['id'];
		$stmt->execute();
		$liked = $stmt->fetch()[0];
		$stmt = $conn->prepare('SELECT COUNT(*) as `likes` FROM `likes` WHERE `imageid` LIKE :id');
		$stmt->bindParam(':id', $id);
		$id = $_GET['id'];
		$stmt->execute();
		$likes = $stmt->fetch()[0];
		$stmt = $conn->prepare('SELECT COUNT(*) as `comments` FROM `comments` WHERE `imageid` LIKE :id');
		$stmt->bindParam(':id', $id);
		$id = $_GET['id'];
		$stmt->execute();
		$comments = $stmt->fetch()[0];
		if ($liked === '0')
		{
			if ($likes === '1' && $comments === '1')
				echo "<div class='image'><div class='divimg'><img class='img' src='images/$id' alt='img'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$id' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
			else if ($likes === '1')
				echo "<div class='image'><div class='divimg'><img class='img' src='images/$id' alt='img'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$id' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
			else if ($comments === '1')
				echo "<div class='image'><div class='divimg'><img class='img' src='images/$id' alt='img'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$id' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
			else
				echo "<div class='image'><div class='divimg'><img class='img' src='images/$id' alt='img'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$id' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
		}
		else
		{
			if ($likes === '1' && $comments === '1')
				echo "<div class='image'><div class='divimg'><img class='img' src='images/$id' alt='img'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$id' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
			else if ($likes === '1')
				echo "<div class='image'><div class='divimg'><img class='img' src='images/$id' alt='img'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$id' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
			else if ($comments === '1')
				echo "<div class='image'><div class='divimg'><img class='img' src='images/$id' alt='img'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$id' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
			else
				echo "<div class='image'><div class='divimg'><img class='img' src='images/$id' alt='img'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$id' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
		}
		echo '<div class="imgAuthor">This image was uploaded by <b>' . $author . '</b>.';
		if ($res[1] === $_SESSION['login'])
		{
			echo ' <a id="del" href="#">Delete</a>';
		}
		echo '</div>';
	}
	catch (PDOException $err)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		echo 'Error: ' . $err->getMessage();
	}
}
else
{
	require_once('back/denyaccess.php');
}