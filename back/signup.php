<?php
if (((isset($_POST['login']) && $_POST['login'] !== '') && (isset($_POST['email']) && $_POST['email'] !== '') && (isset($_POST['password']) && $_POST['password'] !== '')))
{
	try
	{
		if (preg_match('/^.*@.*\..*$/', $_POST['email']) !== 1 || preg_match('/^.{5,25}$/', $_POST['email']) !== 1)
		{
			echo '<script src="scripts/emailnotcorrect.js" ></script>';
			exit();
		}
		if (preg_match('/^.{3,15}$/', $_POST['login']) !== 1)
		{
			echo '<script src="scripts/loginnotcorrect.js" ></script>';
			exit();
		}
		if (preg_match('/^.{8,20}$/', $_POST['password']) !== 1 || preg_match('/.*[a-z]/', $_POST['password']) !== 1 || preg_match('/.*[A-Z]/', $_POST['password']) !== 1 || preg_match('/.*\d/', $_POST['password']) !== 1)
		{
			echo '<script src="scripts/passwordnotcorrect.js" ></script>';
			exit();
		}
		$confirm = md5(uniqid(rand(), true));
		require_once('config/database.php');
		$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT `email` FROM `users`');
		$stmt->execute();
		$emails = $stmt->fetchAll();
		foreach ($emails as $elems)
		{
			if ($elems[0] === $_POST['email'])
			{
				echo '<script>alert(\'This email is already in use! Please, use another email!\');</script>';
				exit();
			}
		}
		$stmt = $conn->prepare('SELECT `login` FROM `users`');
		$stmt->execute();
		$logins = $stmt->fetchAll();
		foreach ($logins as $elems)
		{
			if ($elems[0] === $_POST['login'])
			{
				echo '<script>alert(\'This login is already in use! Please, use another login!\');</script>';
				exit();
			}
		}
		$protocol = 'https';
		if (!mail($_POST['email'], 'Confirmation email', 'You\'ve successfully registred on Camagru site! Here\'s your confirmation link: ' . $protocol . '://' . $_SERVER['HTTP_HOST'] . '/confirm.php?confirm=' . $confirm))
		{
			echo '<script>alert(\'Error! Can\\\'t send confirmation email now! Please, try to register later.\');</script>';
			exit();
		}
		$stmt = $conn->prepare('INSERT INTO `users` (`login`, `password`, `email`, `confirmed`) VALUES (:login, :password, :email, :confirmed)');
		$stmt->bindParam(':login', $login);
		$stmt->bindParam(':password', $password);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':confirmed', $confirmed);
		$login = $_POST['login'];
		$email = $_POST['email'];
		$confirmed = 0;
		$password = $_POST['password'];
		$password = password_hash($password, PASSWORD_BCRYPT);
		$stmt->execute();
		$stmt = $conn->prepare('INSERT INTO `confirm` (`login`, `confirm`) VALUES (:login, :confirm)');
		$stmt->bindParam(':login', $login);
		$stmt->bindParam(':confirm', $confirm);
		$login = $_POST['login'];
		$stmt->execute();
		$stmt = $conn->prepare('INSERT INTO `restore` (`login`, `code`) VALUES (:login, :code)');
		$stmt->bindParam(':login', $login);
		$stmt->bindParam(':code', $code);
		$login = $_POST['login'];
		$code = null;
		$stmt->execute();
		$stmt = $conn->prepare('INSERT INTO `modify` (`oldlogin`, `login`, `password`, `email`, `code`) VALUES (:oldlogin, :login, :password, :email, :code)');
		$stmt->bindParam(':oldlogin', $oldlogin);
		$stmt->bindParam(':login', $login);
		$stmt->bindParam(':password', $password);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':code', $code);
		$oldlogin = $_POST['login'];
		$login = null;
		$password = null;
		$email = null;
		$code = null;
		$stmt->execute();
		echo '<script src="scripts/signedup.js"></script>';
	}
	catch (PDOException $err)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		echo 'Error: ' . $err->getMessage();
	}
	$conn = null;
}