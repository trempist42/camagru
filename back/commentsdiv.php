<?php
try
{
	date_default_timezone_set('Europe/Moscow');

	require_once('config/database.php');
	$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare('SELECT `login`, `comment`, `date` FROM `comments` WHERE `imageid` LIKE :imageid ORDER BY `date` DESC');
	$stmt->bindParam(':imageid', $imageid);
	$imageid = $_GET['id'];
	$stmt->execute();
	$comments = $stmt->fetchAll();
	foreach ($comments as $comment)
	{
		$comment[0] = htmlspecialchars($comment[0]);
		$comment[1] = htmlspecialchars($comment[1]);
		$comment[2] = date('d.m.Y H:i', $comment[2]);
		echo "<div class=\"commentDiv\"><h6 class=\"author\">Author: $comment[0]</h6><div class=\"commentText\">$comment[1]</div><h6 class=\"date\">$comment[2]</h6></div>";
	}
}
catch (PDOException $err)
{
	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
	echo 'Error: ' . $err->getMessage();
}