<?php
if ($_SERVER['DOCUMENT_URI'] === '/signup.php')
{
	require_once ('back/signup.php');
}
elseif ($_SERVER['DOCUMENT_URI'] === '/confirm.php')
{
	require_once ('back/confirm.php');
}
elseif ($_SERVER['DOCUMENT_URI'] === '/restore.php')
{
	require_once ('back/restore.php');
}
elseif ($_SERVER['DOCUMENT_URI'] === '/signin.php')
{
	require_once('back/signin.php');
}
elseif ($_SERVER['DOCUMENT_URI'] === '/modify.php')
{
	require_once ('back/modify.php');
}
elseif ($_SERVER['DOCUMENT_URI'] === '/modified.php')
{
	require_once ('back/modified.php');
}
elseif ($_SERVER['DOCUMENT_URI'] === '/photo.php')
{
	require_once ('back/comments.php');
}
elseif ($_SERVER['DOCUMENT_URI'] === '/' || $_SERVER['DOCUMENT_URI'] === '/index.php')
{
	require_once('back/gallery.php');
}