<?php
function cmp($a, $b)
{
	$a = filemtime('images/' . $a);
	$b = filemtime('images/' . $b);
	if ($a == $b)
		return 0;
	else
		return ($a > $b) ? -1 : 1;
}

try
{
	require_once('config/database.php');
	$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$images = scandir("images");
	unset($images[0], $images[1]);
	$images = array_values($images);
	usort($images, 'cmp');
	$len = sizeof($images);
	if ($len !== 0)
	{
		$pagenum = ceil($len / 6);

		echo "<div class='gallery'>";
		if (!isset($_GET['page']) || $_GET['page'] === '1')
		{
		    if ($len > 6)
			    $tmp = 6;
		    else
		        $tmp = $len;
			if (isset($_SESSION['login']))
			{
				for ($i = 0; $i < $tmp; $i++) {
					$stmt = $conn->prepare('SELECT COUNT(*) AS `count` FROM `likes` WHERE `login` LIKE :login AND `imageid` LIKE :id');
					$stmt->bindParam(':login', $login);
					$stmt->bindParam(':id', $id);
					$login = $_SESSION['login'];
					$id = $images[$i];
					$stmt->execute();
					$liked = $stmt->fetch()[0];
					$stmt = $conn->prepare('SELECT COUNT(*) as `likes` FROM `likes` WHERE `imageid` LIKE :id');
					$stmt->bindParam(':id', $id);
					$id = $images[$i];
					$stmt->execute();
					$likes = $stmt->fetch()[0];
					$stmt = $conn->prepare('SELECT COUNT(*) as `comments` FROM `comments` WHERE `imageid` LIKE :id');
					$stmt->bindParam(':id', $id);
					$id = $images[$i];
					$stmt->execute();
					$comments = $stmt->fetch()[0];
					if ($liked === '0')
					{
						if ($likes === '1') {
							if ($comments === '1')
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
							else
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						} else {
							if ($comments === '1')
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
							else
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						}
					} else {
						if ($likes === '1') {
							if ($comments === '1')
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
							else
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						} else {
							if ($comments === '1')
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
							else
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						}
					}
				}
				echo '</div>';

				if ($pagenum > 1) {
					echo '<div class="pagination">';
					echo '<a class="active" href="#">1</a>';
					if ($pagenum <= 5) {
						for ($i = 1; $i < $pagenum; $i++) {
							$tmp = $i + 1;
							echo "<a href=\".?page=$tmp\">$tmp</a>";
						}
					} else {
						for ($i = 1; $i < 5; $i++) {
							$tmp = $i + 1;
							echo "<a href=\".?page=$tmp\">$tmp</a>";
						}
						echo "<a href=\".?page=$pagenum\">»</a>";
					}
					echo '</div>';
				}
			} else
			{
				for ($i = 0; $i < $tmp; $i++) {
					$stmt = $conn->prepare('SELECT COUNT(*) as `likes` FROM `likes` WHERE `imageid` LIKE :id');
					$stmt->bindParam(':id', $id);
					$id = $images[$i];
					$stmt->execute();
					$likes = $stmt->fetch()[0];
					$stmt = $conn->prepare('SELECT COUNT(*) as `comments` FROM `comments` WHERE `imageid` LIKE :id');
					$stmt->bindParam(':id', $id);
					$id = $images[$i];
					$stmt->execute();
					$comments = $stmt->fetch()[0];
					if ($likes === '1') {
						if ($comments === '1')
							echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						else
							echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
					} else {
						if ($comments === '1')
							echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						else
							echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
					}
				}
				echo '</div>';

				if ($pagenum > 1) {
					echo '<div class="pagination">';
					echo '<a class="active" href="#">1</a>';
					if ($pagenum <= 5) {
						for ($i = 1; $i < $pagenum; $i++) {
							$tmp = $i + 1;
							echo "<a href=\".?page=$tmp\">$tmp</a>";
						}
					} else {
						for ($i = 1; $i < 5; $i++) {
							$tmp = $i + 1;
							echo "<a href=\".?page=$tmp\">$tmp</a>";
						}
						echo "<a href=\".?page=$pagenum\">»</a>";
					}
					echo '</div>';
				}
			}
		} elseif ($_GET['page'] > $pagenum || $_GET['page'] < '1')
		{
			header('Location: /');
		} else
		{
			$page = $_GET['page'];
			if (isset($_SESSION['login']))
			{
				for ($i = ($page - 1) * 6; $i < (($page - 1) * 6) + 6 && $i < $len; $i++) {
					$stmt = $conn->prepare('SELECT COUNT(*) AS `count` FROM `likes` WHERE `login` LIKE :login AND `imageid` LIKE :id');
					$stmt->bindParam(':login', $login);
					$stmt->bindParam(':id', $id);
					$login = $_SESSION['login'];
					$id = $images[$i];
					$stmt->execute();
					$liked = $stmt->fetch()[0];
					$stmt = $conn->prepare('SELECT COUNT(*) as `likes` FROM `likes` WHERE `imageid` LIKE :id');
					$stmt->bindParam(':id', $id);
					$id = $images[$i];
					$stmt->execute();
					$likes = $stmt->fetch()[0];
					$stmt = $conn->prepare('SELECT COUNT(*) as `comments` FROM `comments` WHERE `imageid` LIKE :id');
					$stmt->bindParam(':id', $id);
					$id = $images[$i];
					$stmt->execute();
					$comments = $stmt->fetch()[0];
					if ($liked === '0') {
						if ($likes === '1') {
							if ($comments === '1')
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
							else
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						} else {
							if ($comments === '1')
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
							else
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						}
					} else {
						if ($likes === '1') {
							if ($comments === '1')
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
							else
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						} else {
							if ($comments === '1')
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
							else
								echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/liked.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						}
					}
				}
				echo '</div>';
				echo '<div class="pagination">';
				if ((int)$page > 3) {
					$tmp = $page - 3;
					echo "<a href=\".?page=1\">«</a>";
				}
				if ($page === '2') {
					for ($i = 1; $i < $page; $i++) {
						echo "<a href=\".?page=$i\">$i</a>";
					}
				} else {
					for ($i = $page - 2; $i < $page; $i++) {
						echo "<a href=\".?page=$i\">$i</a>";
					}
				}
				echo "<a class=\"active\" href=\"#\">$page</a>";
				if ($pagenum <= 5) {
					for ($i = $page; $i < $pagenum; $i++) {
						$tmp = $i + 1;
						echo "<a href=\".?page=$tmp\">$tmp</a>";
					}
				} else {
					$tmp = $i;
					for ($i = $page; $i < $page + 2 && $i < $pagenum && $tmp < $pagenum; $i++) {
						$tmp = $i + 1;
						echo "<a href=\".?page=$tmp\">$tmp</a>";
					}
					if ($pagenum - $page > 2) {
						echo "<a href=\".?page=$pagenum\">»</a>";
					}
				}
				echo '</div>';
			} else
			{
				for ($i = ($page - 1) * 6; $i < (($page - 1) * 6) + 6 && $i < $len; $i++) {
					$stmt = $conn->prepare('SELECT COUNT(*) as `likes` FROM `likes` WHERE `imageid` LIKE :id');
					$stmt->bindParam(':id', $id);
					$id = $images[$i];
					$stmt->execute();
					$likes = $stmt->fetch()[0];
					$stmt = $conn->prepare('SELECT COUNT(*) as `comments` FROM `comments` WHERE `imageid` LIKE :id');
					$stmt->bindParam(':id', $id);
					$id = $images[$i];
					$stmt->execute();
					$comments = $stmt->fetch()[0];
					if ($likes === '1') {
						if ($comments === '1')
							echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						else
							echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes like</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
					} else {
						if ($comments === '1')
							echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comment</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
						else
							echo "<div class='image'><div class='divimg'><img class='img' src='images/$images[$i]' alt='img$i'></div><div class=\"imageadds\"><div class='like'><img class='likeimg' id='$images[$i]' src='icons/like.svg' alt='like'></div><div class=\"addition\"><div class='comments'>$comments comments</div><div class='likes'>$likes likes</div></div><div class='comment'><img class='commentimg' src='icons/comment.svg' alt='comment'></div></div></div>";
					}
				}
				echo '</div>';
				echo '<div class="pagination">';
				if ((int)$page > 3) {
					$tmp = $page - 3;
					echo "<a href=\".?page=1\">«</a>";
				}
				if ($page === '2') {
					for ($i = 1; $i < $page; $i++) {
						echo "<a href=\".?page=$i\">$i</a>";
					}
				} else {
					for ($i = $page - 2; $i < $page; $i++) {
						echo "<a href=\".?page=$i\">$i</a>";
					}
				}
				echo "<a class=\"active\" href=\"#\">$page</a>";
				if ($pagenum <= 5) {
					for ($i = $page; $i < $pagenum; $i++) {
						$tmp = $i + 1;
						echo "<a href=\".?page=$tmp\">$tmp</a>";
					}
				} else {
					$tmp = $i;
					for ($i = $page; $i < $page + 2 && $i < $pagenum && $tmp < $pagenum; $i++) {
						$tmp = $i + 1;
						echo "<a href=\".?page=$tmp\">$tmp</a>";
					}
					if ($pagenum - $page > 2) {
						echo "<a href=\".?page=$pagenum\">»</a>";
					}
				}
				echo '</div>';
			}
		}
	}
	else
	{
		echo 'There are no photos yet. Your photo can be first!';
	}
}
catch (PDOException $err)
{
	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
	echo 'Error: ' . $err->getMessage();
}