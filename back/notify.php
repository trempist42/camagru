<?php
try
{
	require_once('config/database.php');
	$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare('SELECT `notify` FROM `users` WHERE `login` LIKE :login');
	$stmt->bindParam(':login', $login);
	if (isset($_SESSION['login']))
		$login = $_SESSION['login'];
	elseif (isset($_GET['code']) && $_GET['code'] !== '')
	{
		$stmt = $conn->prepare('SELECT `login` FROM `restore` WHERE `code` LIKE :confirm');
		$stmt->bindParam(':confirm', $confirm);
		$confirm = $_GET['code'];
		$stmt->execute();
		$user = $stmt->fetch()[0];
		$stmt = $conn->prepare('SELECT `notify` FROM `users` WHERE `login` LIKE :login');
		$stmt->bindParam(':login', $login);
		$login = $user;
	}
	else
		require_once ('back/denyaccess.php');
	$stmt->execute();
	$notify = $stmt->fetch()[0];
	if ($notify === '1')
		echo '<input type="checkbox" name="notify" id="notify" checked>';
	else
		echo '<input type="checkbox" name="notify" id="notify">';
}
catch (PDOException $err)
{
	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
	echo 'Error: ' . $err->getMessage();
}
$conn = null;