<?php
if (isset($_SESSION['login']) && isset($_GET['code']) && $_GET['code'] !== '')
{
	echo '<script>alert(\'You can\\\'t restore account while logged in! Please, log out first.\'); location.href=\'.\'</script>';
	exit();
}
elseif (isset($_SESSION['login']))
{
	if (isset($_POST['submit']) && isset($_POST['oldpassword']))
	{
		try
		{
			if ((isset($_POST['email']) && $_POST['email'] !== '') && (preg_match('/^.*@.*\..*$/', $_POST['email']) !== 1 || preg_match('/^.{5,25}$/', $_POST['email']) !== 1))
			{
				echo '<script src="scripts/emailnotcorrect.js" ></script>';
				exit();
			}
			if ((isset($_POST['login']) && $_POST['login'] !== '') && (preg_match('/^.{3,15}$/', $_POST['login']) !== 1))
			{
				echo '<script src="scripts/loginnotcorrect.js" ></script>';
				exit();
			}
			if ((isset($_POST['password']) && $_POST['password'] !== '') && (preg_match('/^.{8,20}$/', $_POST['password']) !== 1 || preg_match('/.*[a-z]/', $_POST['password']) !== 1 || preg_match('/.*[A-Z]/', $_POST['password']) !== 1 || preg_match('/.*\d/', $_POST['password']) !== 1))
			{
				echo '<script src="scripts/passwordnotcorrect.js" ></script>';
				exit();
			}
			require_once('config/database.php');
			$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$stmt = $conn->prepare('SELECT `email`, `password` FROM `users` WHERE `login` LIKE :login');
			$stmt->bindParam(':login', $login);
			$login = $_SESSION['login'];
			$stmt->execute();
			$mas = $stmt->fetch();
			$oldemail = $mas[0];
			$oldpassword = $mas[1];
			if (!password_verify($_POST['oldpassword'], $oldpassword))
			{
				echo '<script>alert(\'Your old password is not correct!\');</script>';
				exit();
			}
			unset($mas);
			if ((isset($_POST['email']) && $_POST['email'] !== ''))
			{
				$stmt = $conn->prepare('SELECT `email` FROM `users`');
				$stmt->execute();
				$emails = $stmt->fetchAll();
				foreach ($emails as $elems)
				{
					if ($elems[0] === $_POST['email'])
					{
						echo '<script>alert(\'This email is already in use! Please, use another email!\');</script>';
						exit();
					}
				}
			}
			if ((isset($_POST['login']) && $_POST['login'] !== ''))
			{
				$stmt = $conn->prepare('SELECT `login` FROM `users`');
				$stmt->execute();
				$logins = $stmt->fetchAll();
				foreach ($logins as $elems)
				{
					if ($elems[0] === $_POST['login'])
					{
						echo '<script>alert(\'This login is already in use! Please, use another login!\');</script>';
						exit();
					}
				}
			}
			$confirm = md5(uniqid(rand(), true));
			$protocol = 'https';
			if (!mail($oldemail, 'Account modification', 'You\'ve just modified your account credentials on Camagru site! If you didn\'t do it, just ignore this message. Here\'s your account modification confirmation link: ' . $protocol . '://' . $_SERVER['HTTP_HOST']  . '/modified.php?code=' . $confirm))
			{
				echo '<script>alert(\'Error! Can\\\'t send confirmation email now! Please, try to modify your account later.\');</script>';
				exit();
			}
			$stmt = $conn->prepare('UPDATE `modify` SET `login` = :login, `password` = :password, `email` = :email, `notify` = :notify, `code` = :code WHERE `oldlogin` LIKE :oldlogin');
			$stmt->bindParam(':oldlogin', $oldlogin);
			$stmt->bindParam(':login', $login);
			$stmt->bindParam(':password', $password);
			$stmt->bindParam(':email', $email);
			$stmt->bindParam(':notify', $notify);
			$stmt->bindParam(':code', $code);
			$oldlogin = $_SESSION['login'];
			$login = null;
			$password = null;
			$email = null;
			if (isset($_POST['notify']))
				$notify = 1;
			else
				$notify = 0;
			$code = $confirm;
			if ((isset($_POST['login']) && $_POST['login'] !== ''))
				$login = $_POST['login'];
			if ((isset($_POST['password']) && $_POST['password'] !== ''))
				$password = password_hash($_POST['password'], PASSWORD_BCRYPT);
			if ((isset($_POST['email']) && $_POST['email'] !== ''))
				$email = $_POST['email'];
			$stmt->execute();
			echo '<script>alert(\'We\\\'ve sent confirmation email to your current email address. After confirmation, your account will be modified.\');</script>';
		}
		catch (PDOException $err)
		{
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
			echo 'Error: ' . $err->getMessage();
		}
		$conn = null;
	}
	elseif (isset($_POST['submit']))
	{
		echo '<script>alert(\'Please, enter new email and/or new login and/or new password and confirm your account modification by entering your old password!\');</script>';
	}
}
elseif (isset($_GET['code']) && $_GET['code'] !== '')
{
	try
	{
		require_once('config/database.php');
		$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT `login` FROM `restore` WHERE `code` LIKE :code');
		$stmt->bindParam(':code', $code);
		$code = $_GET['code'];
		$stmt->execute();
		$currentlogin = $stmt->fetch()[0];
		if ($currentlogin === null)
			require_once('back/denyaccess.php');
		echo '<script src="scripts/delete.js"></script>';
		if (isset($_POST['submit']))
		{
			if ((isset($_POST['email']) && $_POST['email'] !== '') && (preg_match('/^.*@.*\..*$/', $_POST['email']) !== 1 || preg_match('/^.{5,25}$/', $_POST['email']) !== 1))
			{
				echo '<script src="scripts/emailnotcorrect.js" ></script>';
				exit();
			}
			if ((isset($_POST['login']) && $_POST['login'] !== '') && (preg_match('/^.{3,15}$/', $_POST['login']) !== 1))
			{
				echo '<script src="scripts/loginnotcorrect.js" ></script>';
				exit();
			}
			if ((isset($_POST['password']) && $_POST['password'] !== '') && (preg_match('/^.{8,20}$/', $_POST['password']) !== 1))
			{
				echo '<script src="scripts/passwordnotcorrect.js" ></script>';
				exit();
			}

			$stmt = $conn->prepare('SELECT `email` FROM `users` WHERE `login` LIKE :login');
			$stmt->bindParam(':login', $login);
			$login = $currentlogin;
			$stmt->execute();
			$oldemail = $stmt->fetch()[0];
			if ((isset($_POST['email']) && $_POST['email'] !== ''))
			{
				$stmt = $conn->prepare('SELECT `email` FROM `users`');
				$stmt->execute();
				$emails = $stmt->fetchAll();
				foreach ($emails as $elems) {
					if ($elems[0] === $_POST['email']) {
						echo '<script>alert(\'This email is already in use! Please, use another email!\');</script>';
						exit();
					}
				}
			}
			if ((isset($_POST['login']) && $_POST['login'] !== ''))
			{
				$stmt = $conn->prepare('SELECT `login` FROM `users`');
				$stmt->execute();
				$logins = $stmt->fetchAll();
				foreach ($logins as $elems) {
					if ($elems[0] === $_POST['login']) {
						echo '<script>alert(\'This login is already in use! Please, use another login!\');</script>';
						exit();
					}
				}
			}
			$confirm = md5(uniqid(rand(), true));
			$protocol = 'https';
			if (!mail($oldemail, 'Account modification', 'You\'ve just modified your account credentials on Camagru site! If you didn\'t do it, just ignore this message. Here\'s your account modification confirmation link: ' . $protocol . '://' . $_SERVER['HTTP_HOST'] . '/modified.php?code=' . $confirm))
			{
				echo '<script>alert(\'Error! Can\\\'t send confirmation email now! Please, try to modify your account later.\');</script>';
				exit();
			}
			$stmt = $conn->prepare('UPDATE `modify` SET `login` = :login, `password` = :password, `email` = :email, `notify` = :notify, `code` = :code WHERE `oldlogin` LIKE :oldlogin');
			$stmt->bindParam(':oldlogin', $oldlogin);
			$stmt->bindParam(':login', $login);
			$stmt->bindParam(':password', $password);
			$stmt->bindParam(':email', $email);
			$stmt->bindParam(':notify', $notify);
			$stmt->bindParam(':code', $code);
			$oldlogin = $_SESSION['login'];
			$login = null;
			$password = null;
			$email = null;
			if (isset($_POST['notify']))
				$notify = 1;
			else
				$notify = 0;
			$code = $confirm;
			if ((isset($_POST['login']) && $_POST['login'] !== ''))
				$login = $_POST['login'];
			if ((isset($_POST['password']) && $_POST['password'] !== ''))
				$password = password_hash($_POST['password'], PASSWORD_BCRYPT);
			if ((isset($_POST['email']) && $_POST['email'] !== ''))
				$email = $_POST['email'];
			$stmt->execute();
			$stmt = $conn->prepare('UPDATE `restore` SET `code` = NULL WHERE `login` LIKE :login');
			$stmt->bindParam(':login', $login);
			$login = $currentlogin;
			$stmt->execute();
			echo '<script>alert(\'We\\\'ve sent confirmation email to your current email address. After confirmation, your account will be modified.\');</script>';
		}
	}
	catch (PDOException $err)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		echo 'Error: ' . $err->getMessage();
	}
	$conn = null;
}
else
{
	require_once('back/denyaccess.php');
}