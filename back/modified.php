<?php
function changeuser($oldLogin, $newLogin, PDO $conn)
{
	$stmt = $conn->prepare('UPDATE `comments` SET `login` = :login WHERE `login` LIKE :oldlogin');
	$stmt->bindParam(':oldlogin', $oldlogin);
	$stmt->bindParam(':login', $login);
	$oldlogin = $oldLogin;
	$login = $newLogin;
	$stmt->execute();
	$stmt = $conn->prepare('UPDATE `confirm` SET `login` = :login WHERE `login` LIKE :oldlogin');
	$stmt->bindParam(':oldlogin', $oldlogin);
	$stmt->bindParam(':login', $login);
	$oldlogin = $oldLogin;
	$login = $newLogin;
	$stmt->execute();
	$stmt = $conn->prepare('UPDATE `gallery` SET `login` = :login WHERE `login` LIKE :oldlogin');
	$stmt->bindParam(':oldlogin', $oldlogin);
	$stmt->bindParam(':login', $login);
	$oldlogin = $oldLogin;
	$login = $newLogin;
	$stmt->execute();
	$stmt = $conn->prepare('UPDATE `restore` SET `login` = :login WHERE `login` LIKE :oldlogin');
	$stmt->bindParam(':oldlogin', $oldlogin);
	$stmt->bindParam(':login', $login);
	$oldlogin = $oldLogin;
	$login = $newLogin;
	$stmt->execute();
	$stmt = $conn->prepare('UPDATE `likes` SET `login` = :login WHERE `login` LIKE :oldlogin');
	$stmt->bindParam(':oldlogin', $oldlogin);
	$stmt->bindParam(':login', $login);
	$oldlogin = $oldLogin;
	$login = $newLogin;
	$stmt->execute();
	if (isset($_SESSION['login']))
		$_SESSION['login'] = $newLogin;
}

if (isset($_GET['code']) && strlen($_GET['code']) === 32 && $_GET['code'] !== 1)
{
	try
	{
		require_once('config/database.php');
		$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT `oldlogin` FROM `modify` WHERE `code` LIKE :code ');
		$stmt->bindParam(':code', $code);
		$code = $_GET['code'];
		$stmt->execute();
		$oldLogin = $stmt->fetch()[0];
		if ($oldLogin === null)
		{
			echo 'This confirmation link is not correct. Go to <a href=".">homepage</a>.';
			exit;
		}
		$stmt = $conn->prepare('SELECT `login`, `password`, `email`, `code`, `notify` FROM `modify` WHERE `oldlogin` LIKE :login');
		$stmt->bindParam(':login', $login);
		$login = $oldLogin;
		$stmt->execute();
		$res = $stmt->fetch();
		if ($res[0] !== null && $res[1] !== null && $res[2] !== null)
		{
			changeuser($oldLogin, $res[0], $conn);
			$stmt = $conn->prepare('UPDATE `users` SET `login` = :login, `password` = :password, `email` = :email WHERE `login` LIKE :oldlogin');
			$stmt->bindParam(':oldlogin', $oldlogin);
			$stmt->bindParam(':login', $login);
			$stmt->bindParam(':password', $password);
			$stmt->bindParam(':email', $email);
			$oldlogin = $oldLogin;
			$login = $res[0];
			$password = $res[1];
			$email = $res[2];
			$stmt->execute();
		}
		elseif ($res[0] !== null && $res[1] !== null)
		{
			changeuser($oldLogin, $res[0], $conn);
			$stmt = $conn->prepare('UPDATE `users` SET `login` = :login, `password` = :password WHERE `login` LIKE :oldlogin');
			$stmt->bindParam(':oldlogin', $oldlogin);
			$stmt->bindParam(':login', $login);
			$stmt->bindParam(':password', $password);
			$oldlogin = $oldLogin;
			$login = $res[0];
			$password = $res[1];
			$stmt->execute();
		}
		elseif ($res[0] !== null && $res[2] !== null)
		{
			changeuser($oldLogin, $res[0], $conn);
			$stmt = $conn->prepare('UPDATE `users` SET `login` = :login, `email` = :email WHERE `login` LIKE :oldlogin');
			$stmt->bindParam(':oldlogin', $oldlogin);
			$stmt->bindParam(':login', $login);
			$stmt->bindParam(':email', $email);
			$oldlogin = $oldLogin;
			$login = $res[0];
			$email = $res[2];
			$stmt->execute();
		}
		elseif ($res[1] !== null && $res[2] !== null)
		{
			$stmt = $conn->prepare('UPDATE `users` SET `password` = :password, `email` = :email WHERE `login` LIKE :oldlogin');
			$stmt->bindParam(':oldlogin', $oldlogin);
			$stmt->bindParam(':password', $password);
			$stmt->bindParam(':email', $email);
			$oldlogin = $oldLogin;
			$password = $res[1];
			$email = $res[2];
			$stmt->execute();
		}
		elseif ($res[0] !== null)
		{
			changeuser($oldLogin, $res[0], $conn);
			$stmt = $conn->prepare('UPDATE `users` SET `login` = :login WHERE `login` LIKE :oldlogin');
			$stmt->bindParam(':oldlogin', $oldlogin);
			$stmt->bindParam(':login', $login);
			$oldlogin = $oldLogin;
			$login = $res[0];
			$stmt->execute();
		}
		elseif ($res[1] !== null)
		{
			$stmt = $conn->prepare('UPDATE `users` SET `password` = :password WHERE `login` LIKE :oldlogin');
			$stmt->bindParam(':oldlogin', $oldlogin);
			$stmt->bindParam(':password', $password);
			$oldlogin = $oldLogin;
			$password = $res[1];
			$stmt->execute();
		}
		elseif ($res[2] !== null)
		{
			$stmt = $conn->prepare('UPDATE `users` SET `email` = :email WHERE `login` LIKE :oldlogin');
			$stmt->bindParam(':oldlogin', $oldlogin);
			$stmt->bindParam(':email', $email);
			$oldlogin = $oldLogin;
			$email = $res[2];
			$stmt->execute();
		}
		if ($res[0] !== null)
		{
			$stmt = $conn->prepare('UPDATE `modify` SET `oldlogin` = :login, `login` = NULL, `password` = NULL, `email` = NULL, `notify` = NULL, `code` = NULL WHERE `oldlogin` LIKE :oldlogin');
			$stmt->bindParam(':oldlogin', $oldlogin);
			$stmt->bindParam(':login', $login);
			$oldlogin = $oldLogin;
			$login = $res[0];
			$stmt->execute();
		}
		else
		{
			$stmt = $conn->prepare('UPDATE `modify` SET `login` = NULL, `password` = NULL, `email` = NULL, `notify` = NULL, `code` = NULL WHERE `oldlogin` LIKE :oldlogin');
			$stmt->bindParam(':oldlogin', $oldlogin);
			$oldlogin = $oldLogin;
			$stmt->execute();
		}
		$stmt = $conn->prepare('UPDATE `users` SET `notify` = :notify WHERE `login` LIKE :oldlogin');
		$stmt->bindParam(':notify', $notify);
		$stmt->bindParam(':oldlogin', $oldlogin);
		$notify = $res[4];
		$oldlogin = $oldLogin;
		$stmt->execute();
		if (isset($_SESSION['login']))
		{
			$login = htmlspecialchars($login);
			echo 'Congratulations, <b>' . $login . '</b>! You\'ve just confirmed your account modification!';
			echo '<script>document.getElementsByClassName(\'loggedin\')[0].innerHTML = document.getElementsByClassName(\'loggedin\')[0].innerHTML.replace("' . $oldLogin . '", "' . $login . '")</script>';
		}
		else
		{
			$login = htmlspecialchars($login);
			echo 'Congratulations, <b>' . $login . '</b>! You\'ve just confirmed your account modification! Now you can <a href="signin.php">login</a>!';
		}
	}
	catch (PDOException $err)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		echo 'Error: ' . $err->getMessage();
	}
	$conn = null;
}
else
{
	echo 'This confirmation link is not correct. Go to <a href="signin.php">homepage</a>.';
}