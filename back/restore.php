<?php
if (isset($_POST['login']) && $_POST['login'] !== '')
{
	try
	{
		require_once('config/database.php');
		$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT `email` FROM `users` WHERE `login` LIKE :login');
		$stmt->bindParam(':login', $login);
		$login = $_POST['login'];
		$stmt->execute();
		$email = $stmt->fetch()[0];
		if ($email === null)
		{
			echo '<script>alert(\'This user does not exist!\');</script>';
			exit();
		}
		$code = md5(uniqid(rand(), true));
		$protocol = 'https';
		if (!mail($email, 'Account retrieval', 'You\'ve asked account retrieval on Camagru site! Here\'s your confirmation link: ' . $protocol . '://' . $_SERVER['HTTP_HOST'] . '/modify.php?code=' . $code))
		{
			echo '<script>alert(\'Error! Can\\\'t send email now! Please, try to restore your password later.\');</script>';
			exit();
		}
		$stmt = $conn->prepare('UPDATE `restore` SET `code` = :code WHERE `login` LIKE :login');
		$stmt->bindParam(':login', $login);
		$stmt->bindParam(':code', $code);
		$login = $_POST['login'];
		$stmt->execute();
		echo '<script src="scripts/restore.js"></script>';
	}
	catch (PDOException $err)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		echo 'Error: ' . $err->getMessage();
	}
	$conn = null;
}