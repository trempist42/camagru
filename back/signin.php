<?php
if (!$_POST)
	exit();
if (!isset($_POST['login']) || $_POST['login'] === '' || !isset($_POST['password']) || $_POST['password'] === '')
{
	echo '<script>alert(\'Please, enter your login and password!\');</script>';
	exit();
}
try
{
	require_once ('config/database.php');
	$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare('SELECT `password`, `confirmed` FROM `users` WHERE `login` LIKE :login ');
	$stmt->bindParam(':login', $login);
	$login = $_POST['login'];
	$stmt->execute();
	$res = $stmt->fetch();
	if ($res === false)
	{
		echo '<script>alert(\'Your login/password combination is not correct!\');</script>';
		exit();
	}
	$hash = $res['password'];
	$password = $_POST['password'];
	if (!password_verify($password, $hash))
	{
		echo '<script>alert(\'Your login/password combination is not correct!\');</script>';
		exit();
	}
	if ($res['confirmed'] !== '1')
	{
		echo '<script>alert(\'You can\\\'t login now! You need to confirm your account first.\');</script>';
		exit();
	}
	$_SESSION['login'] = $_POST['login'];
	echo '<script src="scripts/signin.js"></script>';
}
catch (PDOException $err)
{
	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
	echo 'Error: ' . $err->getMessage();
}
$conn = null;
?>