<?php
if (isset($_GET['confirm']) && strlen($_GET['confirm']) === 32 && $_GET['confirm'] !== 1)
{
	try
	{
		require_once('config/database.php');
		$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT `login` FROM `confirm` WHERE `confirm` LIKE :confirm ');
		$stmt->bindParam(':confirm', $confirm);
		$confirm = $_GET['confirm'];
		$stmt->execute();
		$res = $stmt->fetch()[0];
		if ($res === null)
		{
			echo 'This confirmation link is not correct. Go to <a href=".">homepage</a>.';
			exit;
		}
		$stmt = $conn->prepare('UPDATE `users` SET `confirmed` = 1 WHERE `login` LIKE :login');
		$stmt->bindParam(':login', $login);
		$login = $res;
		$stmt->execute();
		$stmt = $conn->prepare('DELETE FROM `confirm` WHERE `confirm` LIKE :confirm ');
		$stmt->bindParam(':confirm', $confirm);
		$confirm = $_GET['confirm'];
		$stmt->execute();
		$login = htmlspecialchars($login);
		echo 'Congratulations, <b>' . $login . '</b>! You\'ve just confirmed your account! Now you can <a href="signin.php">login</a>!';
	}
	catch (PDOException $err)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		echo 'Error: ' . $err->getMessage();
	}
	$conn = null;
}
else
{
	echo 'This confirmation link is not correct. Go to <a href=".">homepage</a>.';
}