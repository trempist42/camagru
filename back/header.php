<?php
if (!isset($_SESSION['login']))
{
	echo '<div class="h1"></div>';
	echo '<div class="h2"><h1 onclick="location.href=\'/\';">Camagru!</h1></div>';
	echo '<div class="h3"><a href="signin.php">Log in</a></div>';
}
else
{
	try
	{
		require_once('config/database.php');
		$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT COUNT(*) FROM `users` WHERE `login` LIKE :login');
		$stmt->bindParam(':login', $login);
		$login = $_SESSION['login'];
		$stmt->execute();
		$login = $stmt->fetch()[0];
		if ($login !== '1')
		{
			session_destroy();
			require_once('back/denyaccess.php');
		}
		$login = htmlspecialchars($_SESSION['login']);
		echo '<div class="h1"><a href="add.php"><img src="icons/photo.svg" height="32" alt="photo"><p>Add</p></a></div>';
		echo '<div class="h2"><h1 onclick="location.href=\'/\';">Camagru!</h1></div>';
		echo '<div class="h3"><a href="modify.php"><img src="icons/settings.svg" height="32" alt="settings"></a><div class="loggedin">Hello, <b>' . $login . '</b>!<br><a href=".?logout=true">Log out</a></div></div>';
	}
	catch (PDOException $err)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		echo 'Error: ' . $err->getMessage();
	}
}