<?php
function cmp($a, $b)
{
	$a = filemtime('images/' . $a);
	$b = filemtime('images/' . $b);
	if ($a == $b)
		return 0;
	else
		return ($a > $b) ? -1 : 1;
}

try
{
	require_once('config/database.php');
	$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare('SELECT `id` FROM `gallery` WHERE login LIKE :login');
	$stmt->bindParam(':login', $login);
	$login = $_SESSION['login'];
	$stmt->execute();
	$res = $stmt->fetchAll();
	$i = 0;
	$images = [];
	foreach ($res as $image)
	{
		$images[$i] = $image[0];
		$i++;
	}
	usort($images, 'cmp');
	$len = sizeof($images);
	if ($len !== 0)
	{
		$i = 0;
		echo "<div class=\"images\">";
		foreach ($images as $image)
		{
			echo "<div class=\"image\"><div class=\"imagediv\"><img class=\"img\" src=\"images/$image\" alt=\"img$i\"></div></div>";
			$i++;
		}
		echo "</div>";
	}
	else
	{
		echo 'You didn\'t take any photo yet.';
	}
}
catch (PDOException $err)
{
	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
	echo 'Error: ' . $err->getMessage();
}
$conn = null;