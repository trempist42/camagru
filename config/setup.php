<?php
require_once ('database.php');
try
{
	if (!is_dir('../images'))
		if (!mkdir('../images'))
			exit('Error! Can\'t create directory for images!');
    if (!is_dir('../work/tmp'))
        if (!mkdir('../work/tmp'))
            exit('Error! Can\'t create temporary directory!');
    if (!is_dir('../work/tmp/res'))
        if (!mkdir('../work/tmp/res'))
            exit('Error! Can\'t create temporary directory!');
	$conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn->exec('CREATE DATABASE IF NOT EXISTS ' . $DB_NAME);
    $conn->exec('USE ' . $DB_NAME);

	$conn->exec('CREATE TABLE IF NOT EXISTS `users` (
    `login` VARCHAR(15) NOT NULL PRIMARY KEY,
    `password` VARCHAR(60) NOT NULL,
    `email` VARCHAR(25) NOT NULL UNIQUE KEY,
    `notify` BOOLEAN NOT NULL DEFAULT TRUE,
    `confirmed` BOOLEAN NOT NULL DEFAULT FALSE
    )');
	$conn->exec('CREATE TABLE IF NOT EXISTS `confirm` (
    `login` VARCHAR(15) NOT NULL PRIMARY KEY,
    `confirm` VARCHAR(32) NOT NULL UNIQUE KEY
    )');
	$conn->exec('CREATE TABLE IF NOT EXISTS `restore` (
    `login` VARCHAR(15) NOT NULL PRIMARY KEY,
    `code` VARCHAR(32) UNIQUE KEY
    )');
	$conn->exec('CREATE TABLE IF NOT EXISTS `modify` (
    `oldlogin` VARCHAR(15) NOT NULL PRIMARY KEY,
    `login` VARCHAR(15),
    `password` VARCHAR(60),
    `email` VARCHAR(25),
    `notify` BOOLEAN,
    `code` VARCHAR(32) UNIQUE KEY
    )');
	$conn->exec('CREATE TABLE IF NOT EXISTS `gallery` (
    `id` VARCHAR(37) NOT NULL PRIMARY KEY,
    `login` VARCHAR(15) NOT NULL
    )');
	$conn->exec('CREATE TABLE IF NOT EXISTS `comments` (
    `id` INT(255) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `imageid` VARCHAR(37) NOT NULL,
    `login` VARCHAR(15) NOT NULL,
    `comment` VARCHAR(140) NOT NULL,
    `date` BIGINT(8) NOT NULL
    )');
	$conn->exec('CREATE TABLE IF NOT EXISTS `likes` (
    `id` INT(255) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `imageid` VARCHAR(37) NOT NULL,
    `login` VARCHAR(15) NOT NULL
    )');
	echo "Database schema created successfully!";
}
catch (PDOException $err)
{
	echo 'Error: ' . $err->getMessage();
}
$conn = null;