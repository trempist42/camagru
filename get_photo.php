<?php
session_start();
$json = file_get_contents('php://input');
$data = json_decode($json);

if (isset($_SESSION['login']) && isset($data->file))
{
	try
	{
		require_once('config/database.php');
		$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$file = $data->file;
		$realSize = $data->realSize;
		$size = $data->size;
		$sun = $data->superposables->sun;
		$duck = $data->superposables->duck;
		$scaleWidth = $realSize->width / $size->width;
		$scaleHeight = $realSize->height / $size->height;

		$format = preg_replace_callback('/data:image\/(.*?);base64,.*/', function ($matches) {return $matches[1];}, $file);
		$file = preg_replace('/data:image\/.*?;base64,/', '', $file);
		if ($format !== 'jpeg' && $format !== 'jpg' && $format !== 'bmp' && $format !== 'png' && $format !== 'gif')
		{
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
			echo 'Server error! Image format wrong!';
			exit();
		}
		$file = base64_decode($file);
		$tmpname = md5(uniqid(rand(), true));
		$name = $tmpname . '.' . $format;

		if ($sun->set === 1)
		{
			$success = file_put_contents('work/tmp/' . $name, $file);
			if ($success === false)
			{
				header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
				echo 'Server error! Can\'t save image!';
				exit();
			}

			for ($i = 0; $i < 4; $i++) {
				$src1 = new Imagick('work/tmp/' . $name);
				$src2 = new Imagick('work/sun/' . 'sun' . '-' . $i . '.gif');
				$src2->scaleImage($sun->width * $scaleWidth, $sun->height * $scaleHeight);
				$src1->compositeImage($src2, Imagick::COMPOSITE_DEFAULT, $sun->left * $scaleWidth, $sun->top * $scaleHeight);
				$src1->writeImage('work/tmp/res/' . $tmpname . '-' . $i . '.gif');
			}
			if ($duck->set === 1) {
				for ($i = 0; $i < 4; $i++) {
					$src1 = new Imagick('work/tmp/res/' . $tmpname . '-' . $i . '.gif');
					$src2 = new Imagick('work/duck/' . 'duck' . '-' . $i . '.gif');
					$src2->scaleImage($duck->width * $scaleWidth, $duck->height * $scaleHeight);
					$src1->compositeImage($src2, Imagick::COMPOSITE_DEFAULT, $duck->left * $scaleWidth, $duck->top * $scaleHeight);
					$src1->writeImage('work/tmp/res/' . $tmpname . '-' . $i . '.gif');
				}
			}
			$files = glob('work/tmp/res/' . $tmpname . '-*.gif');
			natsort($files);
			$resIMG = new Imagick();
			$resIMG->setFormat('gif');
			foreach ($files as $file) {
				$resIMG->addImage(new Imagick($file));
				$resIMG->setImageDelay(20);
			}
			$resIMG = $resIMG->deconstructImages();
			$resIMG->setImageIterations(0);

			$resIMG->writeImages('images/' . $tmpname . '.gif', true);
			foreach ($files as $file)
				unlink($file);
			unlink('work/tmp/' . $name);
			$name = $tmpname . '.gif';
		}
		elseif ($duck->set === 1)
		{
			$success = file_put_contents('work/tmp/' . $name, $file);
			if ($success === false)
			{
				header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
				echo 'Server error! Can\'t save image!';
				exit();
			}

			for ($i = 0; $i < 4; $i++) {
				$src1 = new Imagick('work/tmp/' . $name);
				$src2 = new Imagick('work/duck/' . 'duck' . '-' . $i . '.gif');
				$src2->scaleImage($duck->width * $scaleWidth, $duck->height * $scaleHeight);
				$src1->compositeImage($src2, Imagick::COMPOSITE_DEFAULT, $duck->left * $scaleWidth, $duck->top * $scaleHeight);
				$src1->writeImage('work/tmp/res/' . $tmpname . '-' . $i . '.gif');
			}
			$files = glob('work/tmp/res/' . $tmpname . '-*.gif');
			natsort($files);
			$resIMG = new Imagick();
			$resIMG->setFormat('gif');
			foreach ($files as $file) {
				$resIMG->addImage(new Imagick($file));
				$resIMG->setImageDelay(20);
			}
			$resIMG = $resIMG->deconstructImages();
			$resIMG->setImageIterations(0);

			$resIMG->writeImages('images/' . $tmpname . '.gif', true);
			foreach ($files as $file)
				unlink($file);
            unlink('work/tmp/' . $name);
			$name = $tmpname . '.gif';
		}
		else
		{
			$success = file_put_contents('images/' . $name, $file);
			if ($success === false)
			{
				header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
				echo 'Server error! Can\'t save image!';
				exit();
			}
		}
		$stmt = $conn->prepare('INSERT INTO `gallery` (`id`, `login`) VALUES (:id, :login)');
		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':login', $login);
		$id = $name;
		$login = $_SESSION['login'];
		$stmt->execute();
		echo 'Success! Image uploaded!';
	}
	catch (PDOException $err)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		echo 'Error: ' . $err->getMessage();
	}
	catch (ImagickException $e)
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
        echo 'Error: ' . $e->getMessage();
    }
    $conn = null;
}
else
	require_once('back/denyaccess.php');