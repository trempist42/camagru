<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
	<title>Camagru! | Password retrieval</title>
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat" />
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat+Alternates" />
	<link rel="stylesheet" type="text/css" href="styles/style.css" />
	<?php session_start(); if (isset($_GET['logout']) && $_GET['logout'] === 'true') {require_once('back/logout.php');} if (isset($_SESSION['login'])) {require_once('back/denyaccess.php');}?>
</head>
<body>
<div class="wrapper">
	<div class="header">
		<?php require_once('back/header.php');?>
	</div>
	<div class="content">
		<div class="main">
			<h2>Password retrieval:</h2>
			<form method="post">
				Login: <input type="text" name="login" id="login"><br />
				<h6>Remember your password? <a href="signin.php">Sign In</a></h6>
				<input type="submit" name="submit" value="Restore">
			</form>
			<?php require_once('back/main.php'); ?>

		</div>
	</div>
	<div class="footer">
		<h5>Copyright © 2019 rafalmer</h5>
	</div>
</div>
</body>
</html>