<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
	<title>Camagru! | Photo</title>
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat" />
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat+Alternates" />
	<link rel="stylesheet" type="text/css" href="styles/style.css" />
	<link rel="stylesheet" type="text/css" href="styles/comments.css" />
	<script type="text/javascript" src="https://vk.com/js/api/share.js?93" charset="windows-1251"></script>
	<?php session_start(); if (isset($_GET['logout']) && $_GET['logout'] === 'true') {require_once('back/logout.php');}?>
</head>
<body>
<div class="wrapper">
	<div class="header">
		<?php require_once('back/header.php');?>
	</div>
	<div class="content">
		<div class="main">
			<?php require_once('back/main.php');?>
			<div style="align-self: center">
				<script type="text/javascript">
					document.write(VK.Share.button({
						url: window.location.href,
						title: "Camagru! | Photo",
						image: document.getElementsByClassName('img')[0].src,
						noparse: true,
						no_vk_links: 1
					}, {
						type: "round",
						text: "Share"
						}));
				</script>
			</div>
			<div class="commentform">
				<textarea maxlength="140" class="commentarea" readonly=""></textarea>
				<input type="submit" name="submit" value="Comment">
			</div>
			<div class="commentsDiv"> <?php require_once('back/commentsdiv.php');?></div>
		</div>
	</div>
	<div class="footer">
		<h5>Copyright © 2019 rafalmer</h5>
	</div>
</div>
<script type="text/javascript" src="scripts/comments.js"></script>
<?php if (isset($_GET['comment'])) {echo '<script>comment.removeAttribute(\'readonly\');comment.value = \'\';comment.style.color = \'\';comment.focus();</script>';}?>
</body>
</html>