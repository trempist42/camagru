<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
	<title>Camagru! | Account modify</title>
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat" />
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat+Alternates" />
	<link rel="stylesheet" type="text/css" href="styles/style.css" />
	<?php session_start(); if (isset($_GET['logout']) && $_GET['logout'] === 'true') {require_once('back/logout.php');}?>
</head>
<body>
<div class="wrapper">
	<div class="header">
		<?php require_once('back/header.php');?>
	</div>
	<div class="content">
		<div class="main">
			<h2>Account modify:</h2>
			<h3>Here you can modify your account.</h3>
			<form method="post">
				New login: <input type="text" name="login" id="login" value=""><br />
				New email: <input type="email" name="email" id="email" value=""><br />
				New password: <input type="password" name="password" id="password" value=""><br />
				<?php require_once('back/notify.php');?>
				<label for="notify">Notify me about new comments</label><br />
				<div id="oldpass">Old password: <input type="password" name="oldpassword" id="oldpassword" value=""></div>
				<input type="submit" name="submit" value="Modify">
			</form>
			<?php require_once('back/main.php'); ?>

		</div>
	</div>
	<div class="footer">
		<h5>Copyright © 2019 rafalmer</h5>
	</div>
</div>
<script type="text/javascript" src="scripts/modify.js"></script>
</body>
</html>