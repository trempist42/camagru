<?php
session_start();
if (isset($_SESSION['login']) && isset($_POST['like']))
{
	try
	{
		require_once ('config/database.php');
		$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->prepare('SELECT COUNT(*) AS `count` FROM `likes` WHERE `login` LIKE :login AND `imageid` LIKE :id');
		$stmt->bindParam(':login', $login);
		$stmt->bindParam(':id', $id);
		$login = $_SESSION['login'];
		$id = $_POST['like'];
		$stmt->execute();
		$res = $stmt->fetch()[0];
		if ($res === '0')
		{
			$stmt = $conn->prepare('INSERT INTO `likes` (`imageid`, `login`) VALUES (:id, :login)');
			$stmt->bindParam(':login', $login);
			$stmt->bindParam(':id', $id);
			$login = $_SESSION['login'];
			$id = $_POST['like'];
			$stmt->execute();
		}
		else
		{
			$stmt = $conn->prepare('DELETE FROM `likes` WHERE `imageid` LIKE :id AND `login` LIKE :login');
			$stmt->bindParam(':login', $login);
			$stmt->bindParam(':id', $id);
			$login = $_SESSION['login'];
			$id = $_POST['like'];
			$stmt->execute();
		}
	}
	catch (PDOException $err)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		echo 'Error: ' . $err->getMessage();
	}
	$conn = null;
}
elseif (isset($_SESSION['login']) && isset($_POST['imageid']) && isset($_POST['comment']))
{
	try
	{
		require_once ('config/database.php');
		$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $conn->prepare('SELECT `login` FROM `gallery` WHERE `id` LIKE :imageid');
		$stmt->bindParam(':imageid', $imageid);
		$imageid = $_POST['imageid'];
		$stmt->execute();
		$imageAuthor = $stmt->fetch()[0];
		$stmt = $conn->prepare('SELECT `notify`, `email` FROM `users` WHERE `login` LIKE :login');
		$stmt->bindParam(':login', $login);
		$login = $imageAuthor;
		$stmt->execute();
		$res = $stmt->fetch();
		$notify = $res[0];
		$email = $res[1];
		if ($notify === '1')
		{
			$protocol = 'https';
			if (!mail($email, 'New comment', 'Someone has just commented one of your images: ' . $protocol . '://' . $_SERVER['HTTP_HOST']  . '/photo.php?id=' . $_POST['imageid']))
			{
				echo '<script>alert(\'Error! Can\\\'t send notification email to image author now! Please, try to comment later.\');</script>';
				exit();
			}
		}
		$stmt = $conn->prepare('INSERT INTO `comments` (`imageid`, `login`, `comment`, `date`) VALUES (:imageid, :login, :comments, :timedate)');
		$stmt->bindParam(':login', $login);
		$stmt->bindParam(':imageid', $imageid);
		$stmt->bindParam(':comments', $comments);
		$stmt->bindParam(':timedate', $timedate);
		$login = $_SESSION['login'];
		$imageid = $_POST['imageid'];
		$comments = $_POST['comment'];
		$timedate = time();
		$stmt->execute();
		$data = ['author' => $login, 'comment' => $comments, 'date' => $timedate];
		header('Content-type: application/json');
		echo json_encode($data);
	}
	catch (PDOException $err)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		echo 'Error: ' . $err->getMessage();
	}
}
elseif (isset($_SESSION['login']) && isset($_POST['del']) && $_POST['del'] === 'true' && isset($_POST['imageid']))
{
	try
	{
		require_once ('config/database.php');
		$conn = new PDO($DB_DSN . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT COUNT(*) as `count` FROM `gallery` WHERE `login` LIKE :login AND `id` LIKE :imageid');
		$stmt->bindParam(':login', $login);
		$stmt->bindParam(':imageid', $imageid);
		$login = $_SESSION['login'];
		$imageid = $_POST['imageid'];
		$stmt->execute();
		$authorised = $stmt->fetch()[0];
		if ($authorised === '0')
		{
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
			echo 'Error: You\'re not authorised for this!';
			exit();
		}
		$stmt = $conn->prepare('DELETE FROM `gallery` WHERE `id` LIKE :imageid');
		$stmt->bindParam(':imageid', $imageid);
		$imageid = $_POST['imageid'];
		$stmt->execute();
		$stmt = $conn->prepare('DELETE FROM `likes` WHERE `imageid` LIKE :imageid');
		$stmt->bindParam(':imageid', $imageid);
		$imageid = $_POST['imageid'];
		$stmt->execute();
		$stmt = $conn->prepare('DELETE FROM `comments` WHERE `imageid` LIKE :imageid');
		$stmt->bindParam(':imageid', $imageid);
		$imageid = $_POST['imageid'];
		$stmt->execute();
		unlink('images/' . $_POST['imageid']);
		echo 'You\'ve successfully deleted this image!';
	}
	catch (PDOException $err)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		echo 'Error: ' . $err->getMessage();
	}
}
else
	require_once('back/denyaccess.php');